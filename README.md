
Kotlin is a cross-platform, statically typed, general-purpose programming language with type inference. Kotlin is designed to interoperate fully with Java, and the JVM version of Kotlin's standard library depends on the Java Class Library, but type inference allows its syntax to be more concise. 
## Install Kotlin
'sudo apt install openjdk-11-jdk'
'sudo snap install --classic kotlin'

## Run Kotlin 
'kotlinc hello.kt -include-runtime -d hello.jar' 
'java -jar hello.jar'
